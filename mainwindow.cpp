#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSerialPortInfo>
#include <QMessageBox>
#include <QThread>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    main_flags={stopp};

    connect(ui->bConnect,SIGNAL(clicked()),this,SLOT(connectToArduino()));
    connect(ui->bDisconnect,SIGNAL(clicked()),this,SLOT(disconnectArduino()));
    //connect(ui->bFlash,SIGNAL(clicked()),this,SLOT(flash()));
    //connect(this,SIGNAL(arduinoReady(bool)),ui->bFlash,SLOT(setEnabled(bool)));

    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(comboUpdatePorts()));
    connect(ui->comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(port_select()));
    connect(ui->bInicio,SIGNAL(clicked()),this,SLOT(inic()));
    connect(ui->bFin,SIGNAL(clicked()),this,SLOT(fin()));

    search4Ports();

    ui->bFin->setEnabled(false);//los botones se habilitan cuando el arduino está conectado
    ui->bInicio->setEnabled(false);
    ui->spinBox_7->setValue(UMBRAL_DEFECTO);

    ui->spinBox_9->setValue(tiempoRestante/60);//cargo el valor para que lo pueda editar el usuario
    ui->comboBox->setStyleSheet("QComboBox {"
                            "   padding-left: 10px;"
                            "}"
                            );
    ser = new QSerialPort(this);

}

MainWindow::~MainWindow()
{
    ser->close();   //Importante cerrar el puerto al terminar la aplicación.
    MainWindow::fin(); // termina el ensayo actual, si existe alguno.
    delete ui;
}


void MainWindow::flash(){
    emit arduinoReady(false);

    //Despierta el arduino
    ser->write("\r\n");

    // Envia la orden de parpadear
    ser->write("r\r\n");

}

void MainWindow::serialReceived(){
    if(ser->canReadLine()){ //si no alcanzan los caracteres para formar una linea vuelvo (y espero que se llene el buffer)
    QByteArray data = ser->readLine();
    QString str(data);
 if(main_flags==runn) // ui->checkBox->isChecked() &&
    {
        qDebug()<<str;
        QString filename = ui->lineEdit->text();// le agrega el prefijo
        filename += QString::fromLatin1("%1.csv").arg(timestamp);
        QFile f(filename);
        if (f.open(QIODevice::WriteOnly | QIODevice::Append)) {
            QTextStream out(&f);
             if(ui->checkBox_2->isChecked()){
            out << str;
             }
             else{ // si el modo es digital, guardo los valores de spinbox (que ahora son digitales)
             out << ui->spinBox->value() << ',' << ui->spinBox_2->value() << ','<< ui->spinBox_3->value() << ',';
             out << ui->spinBox_4->value() << ',' << ui->spinBox_5->value() << ','<< ui->spinBox_6->value() << "\r\n";
             }
            f.close();
        }
    }

    QSlider *sliders_list[6];
    //  QCheckBox *check_boxes_list[6];
    QSpinBox *spin_boxes_list[6];
    sliders_list[0]=ui->verticalSlider; //asocio los punteros a los Sliders de la GUI
    sliders_list[1]=ui->verticalSlider_2;
    sliders_list[2]=ui->verticalSlider_3;
    sliders_list[3]=ui->verticalSlider_4;
    sliders_list[4]=ui->verticalSlider_5;
    sliders_list[5]=ui->verticalSlider_6;
    spin_boxes_list[0]=ui->spinBox;
    spin_boxes_list[1]=ui->spinBox_2;
    spin_boxes_list[2]=ui->spinBox_3;
    spin_boxes_list[3]=ui->spinBox_4;
    spin_boxes_list[4]=ui->spinBox_5;
    spin_boxes_list[5]=ui->spinBox_6;

    ui->doubleSpinBox->setValue(tiempos[0]); //muestro los tiempos acumulados.
    ui->doubleSpinBox_2->setValue(tiempos[1]); //muestro los tiempos acumulados.
    ui->doubleSpinBox_3->setValue(tiempos[2]); //muestro los tiempos acumulados.
    ui->doubleSpinBox_4->setValue(tiempos[3]); //muestro los tiempos acumulados.
    ui->doubleSpinBox_5->setValue(tiempos[4]); //muestro los tiempos acumulados.
    ui->doubleSpinBox_6->setValue(tiempos[5]); //muestro los tiempos acumulados.

    QList<int> valores;
    QStringList list = str.split(",",QString::SkipEmptyParts);

    for(int i=0; i < list.size() ; i++)
        valores.push_back(list.at(i).toInt());

    if(valores.size()==6)//Solamente toma la linea como valida si se pudieron reconocer 6 numeros.
    {
        //qDebug() << valores.at(0);
        for(int i=0; i < list.size() ; i++){
            sliders_list[i]->setValue(valores.at(i)/10.2);
            if(ui->checkBox_2->isChecked()){
                spin_boxes_list[i]->setValue(valores.at(i));
            }
            else{
                spin_boxes_list[i]->setValue(valores.at(i)>ui->verticalSlider_7->value());//seteo el valor en digital (true-false)
            }
            if(valores.at(i) >= ui->verticalSlider_7->value()){
                spin_boxes_list[i]->setStyleSheet("QSpinBox { background-color: lightgreen; }");
            }else{spin_boxes_list[i]->setStyleSheet("QSpinBox { background-color: white; }");
            }
        }

        if(main_flags==runn){
            tiempoRestante-=0.2;// si estoy corriendo decremento el tiempo
            ui->progressBar->setValue(((ui->spinBox_9->value()*60)-tiempoRestante)*100/(ui->spinBox_9->value()*60));// y actualizo la barra de progreso
            ui->label_14->setText("Ejecutando");
        }
        if(tiempoRestante <= 0){MainWindow::fin();} // si llega a cero se termino el ensayo, es como apretar fin.
        ui->spinBox_10->setValue((59+tiempoRestante)/60);

        for(int i=0; i < list.size() ; i++){
            if(valores.at(i) >= ui->verticalSlider_7->value()) {
                if(main_flags==waitt){ //si estaba esperando la primera rata lanza el ensayo
                    main_flags=runn;
                    //cambio el color del texto usado para informar el estado
                    QPlainTextEdit edit;
                    QPalette p = edit.palette();
                    p.setColor(QPalette::All, QPalette::Base, Qt::green);
                    edit.setPalette(p);
                    //edit.show();
                    ui->plainTextEdit->setPalette(p);
                    //acá termina el cambio de color
                }
            }
        }

        if(main_flags==runn){//escribiendo el codigo de esta manera evito que el flag cambie entre dato y dato, truncando la primera o ultima medicion.
            for(int i=0; i < list.size() ; i++){
                if(valores.at(i) >= ui->verticalSlider_7->value()) {tiempos[i]+=SAMPLE_T;}//si supera el umbral incrementa el tiempo de esa jaula
        }
        }


    }
    }

}

void MainWindow::comboUpdatePorts(){
    ui->comboBox->clear();
    search4Ports();
}

void MainWindow::search4Ports(){
    QList <QSerialPortInfo> list;
    list = QSerialPortInfo::availablePorts();
    for (int i = 0; i < list.size(); i++) {
        qDebug () << list[i].portName() + " | " + list[i].description();
        ui->comboBox->addItem(list[i].portName());
    }
    if(list.size() != 0){}
    else {portName = ' ';}
}

void MainWindow::port_select(){
    portName = ui->comboBox->currentText(); //permite seleccionar el puerto a posteriori

    //  index_port_select=ui->comboBox->currentIndex(); //guardo la posicion seleccionada para restaurarla despues de actualizar los puertos disponibles
}

void MainWindow::connectToArduino() {
    ui->comboBox->setDisabled(1); // una vez que abri el puerto  no se puede elegir mas.
    QSerialPortInfo *sinfo;

    // Si el puerto ya está abierto, salir.
    if (ser->isOpen()) {
        qDebug() << "Port already open!";
        return;
    }

    connect(ser, SIGNAL(readyRead()), this,SLOT(serialReceived()));

    // Configura el puerto
    ser->setPortName(portName);
    ser->setBaudRate(QSerialPort::Baud9600);
    ser->setDataBits(QSerialPort::Data8);
    ser->setParity(QSerialPort::NoParity);
    ser->setStopBits(QSerialPort::OneStop);
    ser->setFlowControl(QSerialPort::NoFlowControl);

    // Obtengo información del puerto
    sinfo = new QSerialPortInfo (portName);

    qDebug() << "isBusy:" << sinfo->isBusy();
    qDebug() << "isNull:" <<sinfo->isNull();
    qDebug() << "isValid:" << sinfo->isValid();
    qDebug() << "isOpen:" << ser->isOpen();
    qDebug() << "isWritable:" << ser->isWritable();
    qDebug() << "isReadable:" << ser->isReadable();
    qDebug() << "error:" << ser->error();

    // Abro el puerto
    if (!ser->open(QIODevice::ReadWrite)) {
        QMessageBox::critical(this, "Error", ser->errorString());
        qDebug() << ("Error found!");
        ui->comboBox->setStyleSheet("QComboBox { background-color: crimson; }");
    } else {
        qDebug() << ("Connecting...");
        ui->comboBox->setStyleSheet("QComboBox { background-color: lightgreen; }");//marca con verde el combo box si la conexión es correcta
        ui->bFin->setEnabled(true);//los botones se habilitan cuando el arduino está conectado
        ui->bInicio->setEnabled(true);

    }

    // más información de depuración
    qDebug() << "--------------------------";
    qDebug() << "isBusy:" << sinfo->isBusy();
    qDebug() << "isNull:" <<sinfo->isNull();
    qDebug() << "isValid:" << sinfo->isValid();
    qDebug() << "isOpen:" << ser->isOpen();
    qDebug() << "isWritable:" << ser->isWritable();
    qDebug() << "isReadable:" << ser->isReadable();
    qDebug() << "error:" << ser->error();
}

void MainWindow::disconnectArduino() {
    ui->comboBox->setDisabled(0); // una vez que cerré el puerto  se puede de nuevo.
    //ui->comboBox->setStyleSheet("QComboBox { background-color: default; }");
    ui->bFin->setEnabled(false);//los botones se habilitan cuando el arduino está conectado
    ui->bInicio->setEnabled(false);

    if(!ser->isOpen()) {
        qDebug() <<("Already disconnected!");
        return;
    }

    ser->close();
    emit arduinoReady(false);
    qDebug() << ("Disconnected");
}

void MainWindow::fin(){
    //ui->checkBox->setEnabled(true);//habilito la opcion de archivo
    ui->checkBox_2->setEnabled(true);//habilito la modificacion de modo.
    ui->lineEdit->setEnabled(true);//habilito la edicion de nombre de archivo

    if(main_flags==runn)// "ui->checkBox_3->isChecked() &&" si está habilitada la opcion guardar resultados y estaba corriendo el ensayo.
    {
        now = QDateTime::currentDateTime();//guarda el archivo con la fecha y la hora actuales.
        //timestamp = now.toString(QLatin1String("yyyyMMdd-hhmm")); // uso el timestamp que seteado al inicio del ensayo
        QString filename = ui->lineEdit->text();// le agrega el prefijo
        QString data;
        filename += QString::fromLatin1("%1.txt").arg(timestamp);
        QFile f(filename);
        if (f.open(QIODevice::WriteOnly | QIODevice::Append)) {
            QTextStream out(&f);
            timestamp = now.toString(QLatin1String("yyyy/MM/dd - hh:mm"));
            out << timestamp;
            out << "\r\nResultados: \r\nTiempos relevados (en segundos) para los sensores 1 al 6 respectivamente:\r\n  ";
           for (int i=0;i<CANT_CANALES;i++){
                out << QString::number(tiempos[i], 'f', 2) << '\t';}
            out << "\r\n";
            out << " Tiempo previsto:"<< '\t' << ui->spinBox_9->value() << " Minutos" << "\r\n";
            out << " Tiempo total:" << '\t'<< QString::number((ui->spinBox_9->value() - tiempoRestante/60), 'f', 2) << " Minutos" ;
            f.close();
        }
        ui->spinBox_9->setEnabled(true); // habilito el seteo del tiempo de ensayo
        //cambio el color del texto usado para informar el estado
        QPlainTextEdit edit;
        QPalette p = edit.palette();
        p.setColor(QPalette::All, QPalette::Base, Qt::red);
        edit.setPalette(p);
        //edit.show();
        ui->plainTextEdit->setPalette(p);
        //acá termina el cambio de color
        ui->label_14->setText("Detenido");
    }
    main_flags=stopp;
}

void MainWindow::inic()
{
    for(uchar i=0;i<CANT_CANALES;i++)
        tiempos[i]=0; //Pongo denuevo todos los contadores a cero

    now = QDateTime::currentDateTime(); // configuro el string para el nombre de archivo
    timestamp = now.toString(QLatin1String("yyyyMMdd-hhmm"));

    tiempoRestante=ui->spinBox_9->value()*60; //cargo el tiempo de ensayo
    ui->spinBox_9->setEnabled(false);//Deshabilito el modificador
    ui->lineEdit->setEnabled(false);//deshabilito la edicion de nombre de archivo
    ui->checkBox_2->setEnabled(false);
    //ui->checkBox->setEnabled(false);
    main_flags=waitt; //seteo el flag para esperar que arranque el ensayo

    //cambio el color del texto usado para informar el estado
    QPlainTextEdit edit;
    QPalette p = edit.palette();
    //p.setColor(QPalette::Active, QPalette::Base, Qt::yellow);
    p.setColor(QPalette::All, QPalette::Base, Qt::yellow);
    edit.setPalette(p);
    //edit.show();
    ui->plainTextEdit->setPalette(p);
    //acá termina el cambio de color
    ui->label_14->setText("Esperando");
}



