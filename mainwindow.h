#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define CANT_CANALES 6
#define SAMPLE_T 0.200 //segundos
#define UMBRAL_DEFECTO 512


#include <QMainWindow>
#include <QtSerialPort/QtSerialPort>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void search4Ports();
    void connectToArduino();
    void disconnectArduino ();
    void serialReceived();
    void flash();
    void port_select();
    void comboUpdatePorts();

void fin();
void inic();

signals:
    void arduinoReady(bool b);

private:
    float tiempos[CANT_CANALES]={0,0,0,0,0,0},tiempoRestante=20*60;
    enum  flags{waitt,runn,stopp} main_flags; //variable para saber si está corriendo o no.
    int     index_port_select=0;
    QDateTime now;//guarda el archivo con la fecha y la hora actuales.
    QString timestamp;
    Ui::MainWindow *ui;
    QString buffer;

    QSerialPort *ser;
    QString portName;
    //bool arduinoBusy;


};

#endif // MAINWINDOW_H
