/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QFrame *frame_2;
    QFrame *line;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_16;
    QPushButton *bInicio;
    QPushButton *bFin;
    QSplitter *splitter;
    QPushButton *pushButton;
    QComboBox *comboBox;
    QPushButton *bConnect;
    QPushButton *bDisconnect;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_5;
    QPlainTextEdit *plainTextEdit;
    QLabel *label_13;
    QLabel *label_14;
    QProgressBar *progressBar;
    QFrame *line_2;
    QLabel *label_16;
    QLineEdit *lineEdit;
    QFrame *frame_3;
    QWidget *layoutWidget_a;
    QVBoxLayout *verticalLayout_11;
    QVBoxLayout *verticalLayout_9;
    QLabel *label_9;
    QFormLayout *formLayout;
    QLabel *label_10;
    QSpinBox *spinBox_9;
    QLabel *label_8;
    QFormLayout *formLayout_2;
    QSpinBox *spinBox_10;
    QLabel *label_11;
    QFrame *frame_4;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QSlider *verticalSlider;
    QSpinBox *spinBox;
    QLabel *label;
    QVBoxLayout *verticalLayout_2;
    QSlider *verticalSlider_2;
    QSpinBox *spinBox_2;
    QLabel *label_2;
    QVBoxLayout *verticalLayout_3;
    QSlider *verticalSlider_3;
    QSpinBox *spinBox_3;
    QLabel *label_3;
    QVBoxLayout *verticalLayout_4;
    QSlider *verticalSlider_4;
    QSpinBox *spinBox_4;
    QLabel *label_4;
    QVBoxLayout *verticalLayout_5;
    QSlider *verticalSlider_5;
    QSpinBox *spinBox_5;
    QLabel *label_5;
    QVBoxLayout *verticalLayout_6;
    QSlider *verticalSlider_6;
    QSpinBox *spinBox_6;
    QLabel *label_6;
    QVBoxLayout *verticalLayout_7;
    QSlider *verticalSlider_7;
    QSpinBox *spinBox_7;
    QLabel *label_7;
    QWidget *layoutWidget3;
    QHBoxLayout *horizontalLayout_6;
    QDoubleSpinBox *doubleSpinBox;
    QDoubleSpinBox *doubleSpinBox_2;
    QDoubleSpinBox *doubleSpinBox_3;
    QDoubleSpinBox *doubleSpinBox_4;
    QDoubleSpinBox *doubleSpinBox_5;
    QDoubleSpinBox *doubleSpinBox_6;
    QLabel *label_15;
    QCheckBox *checkBox_2;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(770, 460);
        MainWindow->setMinimumSize(QSize(770, 460));
        MainWindow->setMaximumSize(QSize(770, 460));
        QIcon icon;
        icon.addFile(QStringLiteral("rat-silhouette.png"), QSize(), QIcon::Normal, QIcon::On);
        MainWindow->setWindowIcon(icon);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        frame_2 = new QFrame(centralWidget);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(10, 10, 751, 81));
        frame_2->setFrameShape(QFrame::Box);
        frame_2->setFrameShadow(QFrame::Raised);
        line = new QFrame(frame_2);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(470, 10, 3, 61));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        layoutWidget = new QWidget(frame_2);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(480, 10, 82, 62));
        verticalLayout_16 = new QVBoxLayout(layoutWidget);
        verticalLayout_16->setSpacing(6);
        verticalLayout_16->setContentsMargins(11, 11, 11, 11);
        verticalLayout_16->setObjectName(QStringLiteral("verticalLayout_16"));
        verticalLayout_16->setContentsMargins(0, 0, 0, 0);
        bInicio = new QPushButton(layoutWidget);
        bInicio->setObjectName(QStringLiteral("bInicio"));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(138, 226, 52, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(198, 255, 143, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(168, 240, 97, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(69, 113, 26, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(92, 151, 35, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush6(QColor(255, 255, 255, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        QBrush brush7(QColor(196, 240, 153, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush7);
        QBrush brush8(QColor(255, 255, 220, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush9(QColor(0, 0, 0, 128));
        brush9.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
#endif
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush9);
#endif
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush9);
#endif
        bInicio->setPalette(palette);
        QFont font;
        font.setFamily(QStringLiteral("Cantarell"));
        bInicio->setFont(font);

        verticalLayout_16->addWidget(bInicio);

        bFin = new QPushButton(layoutWidget);
        bFin->setObjectName(QStringLiteral("bFin"));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush10(QColor(239, 41, 41, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush10);
        QBrush brush11(QColor(255, 147, 147, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush11);
        QBrush brush12(QColor(247, 94, 94, 255));
        brush12.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush12);
        QBrush brush13(QColor(119, 20, 20, 255));
        brush13.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush13);
        QBrush brush14(QColor(159, 27, 27, 255));
        brush14.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush14);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush10);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush);
        QBrush brush15(QColor(247, 148, 148, 255));
        brush15.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush15);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
#endif
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush12);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush13);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush14);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush15);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush9);
#endif
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush12);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush14);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush9);
#endif
        bFin->setPalette(palette1);

        verticalLayout_16->addWidget(bFin);

        splitter = new QSplitter(frame_2);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setGeometry(QRect(10, 10, 131, 25));
        splitter->setOrientation(Qt::Horizontal);
        pushButton = new QPushButton(splitter);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        QIcon icon1;
        QString iconThemeName = QStringLiteral("default");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QStringLiteral("refresh_512.png"), QSize(), QIcon::Normal, QIcon::Off);
        }
        pushButton->setIcon(icon1);
        splitter->addWidget(pushButton);
        comboBox = new QComboBox(splitter);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(comboBox->sizePolicy().hasHeightForWidth());
        comboBox->setSizePolicy(sizePolicy);
        comboBox->setAutoFillBackground(false);
        comboBox->setEditable(false);
        comboBox->setInsertPolicy(QComboBox::InsertAtBottom);
        comboBox->setIconSize(QSize(16, 16));
        comboBox->setFrame(false);
        splitter->addWidget(comboBox);
        bConnect = new QPushButton(frame_2);
        bConnect->setObjectName(QStringLiteral("bConnect"));
        bConnect->setGeometry(QRect(10, 40, 80, 27));
        bDisconnect = new QPushButton(frame_2);
        bDisconnect->setObjectName(QStringLiteral("bDisconnect"));
        bDisconnect->setGeometry(QRect(100, 40, 101, 27));
        layoutWidget1 = new QWidget(frame_2);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(580, 10, 157, 32));
        horizontalLayout_5 = new QHBoxLayout(layoutWidget1);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        plainTextEdit = new QPlainTextEdit(layoutWidget1);
        plainTextEdit->setObjectName(QStringLiteral("plainTextEdit"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(plainTextEdit->sizePolicy().hasHeightForWidth());
        plainTextEdit->setSizePolicy(sizePolicy1);
        plainTextEdit->setMinimumSize(QSize(5, 5));
        plainTextEdit->setMaximumSize(QSize(30, 30));
        plainTextEdit->setBaseSize(QSize(5, 12));
        plainTextEdit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        plainTextEdit->setReadOnly(true);

        horizontalLayout_5->addWidget(plainTextEdit);

        label_13 = new QLabel(layoutWidget1);
        label_13->setObjectName(QStringLiteral("label_13"));

        horizontalLayout_5->addWidget(label_13);

        label_14 = new QLabel(layoutWidget1);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_5->addWidget(label_14);

        progressBar = new QProgressBar(frame_2);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(580, 50, 157, 27));
        progressBar->setMaximum(100);
        progressBar->setValue(0);
        line_2 = new QFrame(frame_2);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(260, 10, 3, 61));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        label_16 = new QLabel(frame_2);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setGeometry(QRect(280, 10, 121, 19));
        lineEdit = new QLineEdit(frame_2);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(280, 40, 161, 31));
        lineEdit->setMaxLength(25);
        frame_3 = new QFrame(centralWidget);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setGeometry(QRect(580, 100, 181, 211));
        frame_3->setFrameShape(QFrame::Box);
        frame_3->setFrameShadow(QFrame::Raised);
        layoutWidget_a = new QWidget(frame_3);
        layoutWidget_a->setObjectName(QStringLiteral("layoutWidget_a"));
        layoutWidget_a->setGeometry(QRect(10, 10, 161, 191));
        verticalLayout_11 = new QVBoxLayout(layoutWidget_a);
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setContentsMargins(11, 11, 11, 11);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        verticalLayout_11->setContentsMargins(0, 0, 0, 0);
        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));

        verticalLayout_11->addLayout(verticalLayout_9);

        label_9 = new QLabel(layoutWidget_a);
        label_9->setObjectName(QStringLiteral("label_9"));

        verticalLayout_11->addWidget(label_9);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label_10 = new QLabel(layoutWidget_a);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout->setWidget(0, QFormLayout::FieldRole, label_10);

        spinBox_9 = new QSpinBox(layoutWidget_a);
        spinBox_9->setObjectName(QStringLiteral("spinBox_9"));
        spinBox_9->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_9->setAlignment(Qt::AlignCenter);
        spinBox_9->setReadOnly(false);
        spinBox_9->setButtonSymbols(QAbstractSpinBox::UpDownArrows);
        spinBox_9->setMinimum(1);
        spinBox_9->setMaximum(120);
        spinBox_9->setValue(20);

        formLayout->setWidget(0, QFormLayout::LabelRole, spinBox_9);


        verticalLayout_11->addLayout(formLayout);

        label_8 = new QLabel(layoutWidget_a);
        label_8->setObjectName(QStringLiteral("label_8"));

        verticalLayout_11->addWidget(label_8);

        formLayout_2 = new QFormLayout();
        formLayout_2->setSpacing(6);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setLabelAlignment(Qt::AlignCenter);
        formLayout_2->setFormAlignment(Qt::AlignCenter);
        spinBox_10 = new QSpinBox(layoutWidget_a);
        spinBox_10->setObjectName(QStringLiteral("spinBox_10"));
        spinBox_10->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_10->setAlignment(Qt::AlignCenter);
        spinBox_10->setReadOnly(true);
        spinBox_10->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_10->setMaximum(9999);
        spinBox_10->setValue(20);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, spinBox_10);

        label_11 = new QLabel(layoutWidget_a);
        label_11->setObjectName(QStringLiteral("label_11"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label_11->sizePolicy().hasHeightForWidth());
        label_11->setSizePolicy(sizePolicy2);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, label_11);


        verticalLayout_11->addLayout(formLayout_2);

        frame_4 = new QFrame(centralWidget);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        frame_4->setGeometry(QRect(10, 100, 551, 321));
        frame_4->setFrameShape(QFrame::Box);
        frame_4->setFrameShadow(QFrame::Raised);
        layoutWidget2 = new QWidget(frame_4);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(10, 10, 531, 231));
        horizontalLayout = new QHBoxLayout(layoutWidget2);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalSlider = new QSlider(layoutWidget2);
        verticalSlider->setObjectName(QStringLiteral("verticalSlider"));
        verticalSlider->setBaseSize(QSize(0, 0));
        verticalSlider->setAutoFillBackground(false);
        verticalSlider->setSliderPosition(0);
        verticalSlider->setOrientation(Qt::Vertical);
        verticalSlider->setTickPosition(QSlider::NoTicks);
        verticalSlider->setTickInterval(10);

        verticalLayout->addWidget(verticalSlider, 0, Qt::AlignHCenter);

        spinBox = new QSpinBox(layoutWidget2);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setEnabled(true);
        sizePolicy1.setHeightForWidth(spinBox->sizePolicy().hasHeightForWidth());
        spinBox->setSizePolicy(sizePolicy1);
        QFont font1;
        font1.setKerning(true);
        spinBox->setFont(font1);
        spinBox->setInputMethodHints(Qt::ImhNone);
        spinBox->setWrapping(false);
        spinBox->setAlignment(Qt::AlignCenter);
        spinBox->setReadOnly(true);
        spinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox->setProperty("showGroupSeparator", QVariant(false));
        spinBox->setMaximum(9999);

        verticalLayout->addWidget(spinBox, 0, Qt::AlignHCenter);

        label = new QLabel(layoutWidget2);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);

        verticalLayout->addWidget(label);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalSlider_2 = new QSlider(layoutWidget2);
        verticalSlider_2->setObjectName(QStringLiteral("verticalSlider_2"));
        verticalSlider_2->setBaseSize(QSize(0, 0));
        verticalSlider_2->setAutoFillBackground(false);
        verticalSlider_2->setSliderPosition(0);
        verticalSlider_2->setOrientation(Qt::Vertical);
        verticalSlider_2->setTickPosition(QSlider::NoTicks);
        verticalSlider_2->setTickInterval(10);

        verticalLayout_2->addWidget(verticalSlider_2, 0, Qt::AlignHCenter);

        spinBox_2 = new QSpinBox(layoutWidget2);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
        sizePolicy1.setHeightForWidth(spinBox_2->sizePolicy().hasHeightForWidth());
        spinBox_2->setSizePolicy(sizePolicy1);
        spinBox_2->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_2->setAlignment(Qt::AlignCenter);
        spinBox_2->setReadOnly(true);
        spinBox_2->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_2->setMaximum(9999);

        verticalLayout_2->addWidget(spinBox_2, 0, Qt::AlignHCenter);

        label_2 = new QLabel(layoutWidget2);
        label_2->setObjectName(QStringLiteral("label_2"));
        sizePolicy2.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy2);

        verticalLayout_2->addWidget(label_2, 0, Qt::AlignHCenter);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalSlider_3 = new QSlider(layoutWidget2);
        verticalSlider_3->setObjectName(QStringLiteral("verticalSlider_3"));
        verticalSlider_3->setBaseSize(QSize(0, 0));
        verticalSlider_3->setAutoFillBackground(false);
        verticalSlider_3->setSliderPosition(0);
        verticalSlider_3->setOrientation(Qt::Vertical);
        verticalSlider_3->setTickPosition(QSlider::NoTicks);
        verticalSlider_3->setTickInterval(10);

        verticalLayout_3->addWidget(verticalSlider_3, 0, Qt::AlignHCenter);

        spinBox_3 = new QSpinBox(layoutWidget2);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));
        sizePolicy1.setHeightForWidth(spinBox_3->sizePolicy().hasHeightForWidth());
        spinBox_3->setSizePolicy(sizePolicy1);
        spinBox_3->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_3->setAlignment(Qt::AlignCenter);
        spinBox_3->setReadOnly(true);
        spinBox_3->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_3->setMaximum(9999);

        verticalLayout_3->addWidget(spinBox_3, 0, Qt::AlignHCenter);

        label_3 = new QLabel(layoutWidget2);
        label_3->setObjectName(QStringLiteral("label_3"));
        sizePolicy2.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy2);

        verticalLayout_3->addWidget(label_3, 0, Qt::AlignHCenter);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalSlider_4 = new QSlider(layoutWidget2);
        verticalSlider_4->setObjectName(QStringLiteral("verticalSlider_4"));
        verticalSlider_4->setBaseSize(QSize(0, 0));
        verticalSlider_4->setAutoFillBackground(false);
        verticalSlider_4->setSliderPosition(0);
        verticalSlider_4->setOrientation(Qt::Vertical);
        verticalSlider_4->setTickPosition(QSlider::NoTicks);
        verticalSlider_4->setTickInterval(10);

        verticalLayout_4->addWidget(verticalSlider_4, 0, Qt::AlignHCenter);

        spinBox_4 = new QSpinBox(layoutWidget2);
        spinBox_4->setObjectName(QStringLiteral("spinBox_4"));
        sizePolicy1.setHeightForWidth(spinBox_4->sizePolicy().hasHeightForWidth());
        spinBox_4->setSizePolicy(sizePolicy1);
        spinBox_4->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_4->setAlignment(Qt::AlignCenter);
        spinBox_4->setReadOnly(true);
        spinBox_4->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_4->setMaximum(9999);

        verticalLayout_4->addWidget(spinBox_4, 0, Qt::AlignHCenter);

        label_4 = new QLabel(layoutWidget2);
        label_4->setObjectName(QStringLiteral("label_4"));
        sizePolicy2.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy2);

        verticalLayout_4->addWidget(label_4, 0, Qt::AlignHCenter);


        horizontalLayout->addLayout(verticalLayout_4);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalSlider_5 = new QSlider(layoutWidget2);
        verticalSlider_5->setObjectName(QStringLiteral("verticalSlider_5"));
        verticalSlider_5->setBaseSize(QSize(0, 0));
        verticalSlider_5->setAutoFillBackground(false);
        verticalSlider_5->setSliderPosition(0);
        verticalSlider_5->setOrientation(Qt::Vertical);
        verticalSlider_5->setTickPosition(QSlider::NoTicks);
        verticalSlider_5->setTickInterval(10);

        verticalLayout_5->addWidget(verticalSlider_5, 0, Qt::AlignHCenter);

        spinBox_5 = new QSpinBox(layoutWidget2);
        spinBox_5->setObjectName(QStringLiteral("spinBox_5"));
        sizePolicy1.setHeightForWidth(spinBox_5->sizePolicy().hasHeightForWidth());
        spinBox_5->setSizePolicy(sizePolicy1);
        spinBox_5->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_5->setAlignment(Qt::AlignCenter);
        spinBox_5->setReadOnly(true);
        spinBox_5->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_5->setMaximum(9999);

        verticalLayout_5->addWidget(spinBox_5, 0, Qt::AlignHCenter);

        label_5 = new QLabel(layoutWidget2);
        label_5->setObjectName(QStringLiteral("label_5"));
        sizePolicy2.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy2);

        verticalLayout_5->addWidget(label_5, 0, Qt::AlignHCenter);


        horizontalLayout->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalSlider_6 = new QSlider(layoutWidget2);
        verticalSlider_6->setObjectName(QStringLiteral("verticalSlider_6"));
        verticalSlider_6->setBaseSize(QSize(0, 0));
        verticalSlider_6->setAutoFillBackground(false);
        verticalSlider_6->setSliderPosition(0);
        verticalSlider_6->setOrientation(Qt::Vertical);
        verticalSlider_6->setTickPosition(QSlider::NoTicks);
        verticalSlider_6->setTickInterval(10);

        verticalLayout_6->addWidget(verticalSlider_6, 0, Qt::AlignHCenter);

        spinBox_6 = new QSpinBox(layoutWidget2);
        spinBox_6->setObjectName(QStringLiteral("spinBox_6"));
        sizePolicy1.setHeightForWidth(spinBox_6->sizePolicy().hasHeightForWidth());
        spinBox_6->setSizePolicy(sizePolicy1);
        spinBox_6->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_6->setAlignment(Qt::AlignCenter);
        spinBox_6->setReadOnly(true);
        spinBox_6->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_6->setMaximum(9999);

        verticalLayout_6->addWidget(spinBox_6, 0, Qt::AlignHCenter);

        label_6 = new QLabel(layoutWidget2);
        label_6->setObjectName(QStringLiteral("label_6"));
        sizePolicy2.setHeightForWidth(label_6->sizePolicy().hasHeightForWidth());
        label_6->setSizePolicy(sizePolicy2);

        verticalLayout_6->addWidget(label_6, 0, Qt::AlignHCenter);


        horizontalLayout->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        verticalSlider_7 = new QSlider(layoutWidget2);
        verticalSlider_7->setObjectName(QStringLiteral("verticalSlider_7"));
        verticalSlider_7->setMouseTracking(true);
        verticalSlider_7->setMaximum(1023);
        verticalSlider_7->setValue(512);
        verticalSlider_7->setOrientation(Qt::Vertical);

        verticalLayout_7->addWidget(verticalSlider_7, 0, Qt::AlignHCenter);

        spinBox_7 = new QSpinBox(layoutWidget2);
        spinBox_7->setObjectName(QStringLiteral("spinBox_7"));
        spinBox_7->setInputMethodHints(Qt::ImhDigitsOnly);
        spinBox_7->setAlignment(Qt::AlignCenter);
        spinBox_7->setReadOnly(true);
        spinBox_7->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_7->setMaximum(9999);

        verticalLayout_7->addWidget(spinBox_7);

        label_7 = new QLabel(layoutWidget2);
        label_7->setObjectName(QStringLiteral("label_7"));
        sizePolicy2.setHeightForWidth(label_7->sizePolicy().hasHeightForWidth());
        label_7->setSizePolicy(sizePolicy2);

        verticalLayout_7->addWidget(label_7, 0, Qt::AlignHCenter);


        horizontalLayout->addLayout(verticalLayout_7);

        layoutWidget3 = new QWidget(frame_4);
        layoutWidget3->setObjectName(QStringLiteral("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(10, 280, 451, 30));
        horizontalLayout_6 = new QHBoxLayout(layoutWidget3);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        doubleSpinBox = new QDoubleSpinBox(layoutWidget3);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setAlignment(Qt::AlignCenter);
        doubleSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        doubleSpinBox->setDecimals(1);
        doubleSpinBox->setMaximum(9.999e+7);

        horizontalLayout_6->addWidget(doubleSpinBox);

        doubleSpinBox_2 = new QDoubleSpinBox(layoutWidget3);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        doubleSpinBox_2->setAlignment(Qt::AlignCenter);
        doubleSpinBox_2->setButtonSymbols(QAbstractSpinBox::NoButtons);
        doubleSpinBox_2->setDecimals(1);
        doubleSpinBox_2->setMaximum(9.999e+7);

        horizontalLayout_6->addWidget(doubleSpinBox_2);

        doubleSpinBox_3 = new QDoubleSpinBox(layoutWidget3);
        doubleSpinBox_3->setObjectName(QStringLiteral("doubleSpinBox_3"));
        doubleSpinBox_3->setAlignment(Qt::AlignCenter);
        doubleSpinBox_3->setButtonSymbols(QAbstractSpinBox::NoButtons);
        doubleSpinBox_3->setDecimals(1);
        doubleSpinBox_3->setMaximum(9.999e+7);

        horizontalLayout_6->addWidget(doubleSpinBox_3);

        doubleSpinBox_4 = new QDoubleSpinBox(layoutWidget3);
        doubleSpinBox_4->setObjectName(QStringLiteral("doubleSpinBox_4"));
        doubleSpinBox_4->setAlignment(Qt::AlignCenter);
        doubleSpinBox_4->setButtonSymbols(QAbstractSpinBox::NoButtons);
        doubleSpinBox_4->setDecimals(1);
        doubleSpinBox_4->setMaximum(9.999e+7);

        horizontalLayout_6->addWidget(doubleSpinBox_4);

        doubleSpinBox_5 = new QDoubleSpinBox(layoutWidget3);
        doubleSpinBox_5->setObjectName(QStringLiteral("doubleSpinBox_5"));
        doubleSpinBox_5->setAlignment(Qt::AlignCenter);
        doubleSpinBox_5->setButtonSymbols(QAbstractSpinBox::NoButtons);
        doubleSpinBox_5->setDecimals(1);
        doubleSpinBox_5->setMaximum(9.999e+7);

        horizontalLayout_6->addWidget(doubleSpinBox_5);

        doubleSpinBox_6 = new QDoubleSpinBox(layoutWidget3);
        doubleSpinBox_6->setObjectName(QStringLiteral("doubleSpinBox_6"));
        doubleSpinBox_6->setAlignment(Qt::AlignCenter);
        doubleSpinBox_6->setButtonSymbols(QAbstractSpinBox::NoButtons);
        doubleSpinBox_6->setDecimals(1);
        doubleSpinBox_6->setMaximum(9.999e+7);

        horizontalLayout_6->addWidget(doubleSpinBox_6);

        label_15 = new QLabel(frame_4);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setGeometry(QRect(20, 250, 241, 19));
        checkBox_2 = new QCheckBox(centralWidget);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setEnabled(true);
        checkBox_2->setGeometry(QRect(740, 410, 16, 21));
        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        mainToolBar->setEnabled(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        statusBar->setEnabled(false);
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);
        QObject::connect(verticalSlider_7, SIGNAL(valueChanged(int)), spinBox_7, SLOT(setValue(int)));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Proy_psico", Q_NULLPTR));
        MainWindow->setWindowFilePath(QString());
        bInicio->setText(QApplication::translate("MainWindow", "Inicio", Q_NULLPTR));
        bFin->setText(QApplication::translate("MainWindow", "Fin", Q_NULLPTR));
        pushButton->setText(QString());
        bConnect->setText(QApplication::translate("MainWindow", "Conectar", Q_NULLPTR));
        bDisconnect->setText(QApplication::translate("MainWindow", "Desconectar", Q_NULLPTR));
        label_13->setText(QApplication::translate("MainWindow", "Estado:", Q_NULLPTR));
        label_14->setText(QString());
        label_16->setText(QApplication::translate("MainWindow", "Nombre Archivo:", Q_NULLPTR));
        label_9->setText(QApplication::translate("MainWindow", "Tiempo Total:", Q_NULLPTR));
        label_10->setText(QApplication::translate("MainWindow", "min.", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindow", "Tiempo Restante", Q_NULLPTR));
        label_11->setText(QApplication::translate("MainWindow", "min.", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        spinBox->setAccessibleName(QString());
#endif // QT_NO_ACCESSIBILITY
        label->setText(QApplication::translate("MainWindow", "Sensor 1", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Sensor 2", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Sensor 3", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Sensor 4", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Sensor 5", Q_NULLPTR));
        label_6->setText(QApplication::translate("MainWindow", "Sensor 6", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "Umbral", Q_NULLPTR));
        label_15->setText(QApplication::translate("MainWindow", "Tiempo acumulado en segundos:", Q_NULLPTR));
        checkBox_2->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        statusBar->setAccessibleDescription(QString());
#endif // QT_NO_ACCESSIBILITY
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
